require("dotenv").config();
const config = require("./config/config");
require("./config/logger").init();
const chalk = require("chalk");

const logger = require("./config/logger");

function loadExpress() {
  const app = require("./config/express")();

  return app;
}

async function connectDatabase() {
  const mongoose = require("mongoose");
  try {
    const db = await mongoose.connect(config.db, {
      useNewUrlParser: true,
      useFindAndModify: true,
      useCreateIndex: true,
      reconnectTries: Number.MAX_VALUE,
      reconnectInterval: 2000,
    });
    logger.info("Connected to MongoDB");
    console.info(chalk.bgWhite.green("connect to MongoDB!"));
    return db;
  } catch (err) {
    console.error(chalk.red("Could not connect to MongoDB!"));
    logger.info(chalk.red(err));
  }
}

async function connectMysql() {
  const { instance } = require("./config/mysql");
  try {
    await instance.authenticate();

    logger.info("Connection has been established successfully!!!");
    console.info(chalk.bgWhite.green("connect to MYSQL!"));
    await instance.sync();
    logger.info("Sync table mysql successfully!!!");
    console.info(chalk.bgWhite.green("Sync table mysql successfully!!!"));
  } catch (err) {
    console.error(chalk.red("Could not connect to MYSQL!"));
    logger.info(chalk.red(err));
  }
}

async function main() {
  const app = loadExpress();

  await connectDatabase();
  await connectMysql();

  app.listen(config.port);
  logger.info(`application started on port ${config.port}`);

  if (process.env.NODE_ENV === "development") {
    console.error(
      chalk.blue.bgWhite.bold(`application started on port ${config.port}`)
    );
  }
}

main();
