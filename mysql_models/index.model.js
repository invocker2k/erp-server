const fs = require("fs");

module.exports = () => {
  fs.readdirSync(__dirname).forEach((file) => {
    if (!file.endsWith(".js") || file === "index.models.js") return;
    require("./" + file);
  });
};
