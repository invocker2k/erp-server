"use strict";

const { Sequelize, instance } = require("../config/mysql");

const Products = instance.define("products", {
  sku: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  price: {
    type: Sequelize.FLOAT,
    allowNull: false,
  },
  salePrice: {
    type: Sequelize.FLOAT,
  },
});

const a = async () => {
  await Products.findAll();
};
a();
// Products.sync({ force: true }).then(() => {
//   console.log("Tao bang Products thanh cong!");
// });
// (async () => {
//   await Products.sync();
//   const jane = await Products.create({
//     sku: "1101",
//     name: "invocker",
//     price : 123

// });
//   console.log(jane.toJSON());
// })();
module.exports = {
  Products,
};
