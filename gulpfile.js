const gulp = require("gulp");
const nodemon = require("gulp-nodemon");
const { series } = gulp;

const paths = {
  serverViews: ["app/views/**/*.*"],
  serverJS: [
    "server.js",
    "config/**/*.js",
    "config/*.js",
    "reoutes/**/*.js",
    "server/**/*.js",
    ".cache-version",
    ".env",
  ],
  nodemonServerJS: [
    "server.js",
    "config",
    "server",
    "routes",
    "models",
    "mysql_models",
    "utils",
    "controllers",
    ".cache-version",
  ],
};

function nodemonWatch() {
  return nodemon({
    script: "server.js",
    nodeArgs: ["--inspect=0.0.0.0:9222"],
    ext: "js",
    watch: paths.nodemonServerJS,
    signal: "SIGINT",
  });
}

exports.default = series(nodemonWatch);
