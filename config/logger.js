const winston = require("winston");
const moment = require("moment");
const _ = require("lodash");
const { combine, timestamp, label, printf } = winston.format;

const infoFormat = printf((info) => {
  let text = `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
  if (info.level === "error") {
    text += `\n${info.url || ""} ${info.user || ""}
  ${info.stack}`;
  }
  return text;
});

const logger = winston.createLogger({
  level: "info",
});

exports.init = function () {
  logger.configure({
    level: "info",
    json: false,
    transports: [
      new winston.transports.File({ filename: "error.log", level: "error" }),
      new winston.transports.File({ filename: "info.log", level: "info" }),
    ],
    format: combine(
      label({ label: "invocker server work" }),
      timestamp({
        format: function () {
          return moment().format("YYYY-MM-DD HH:mm:ss A");
        },
      }),
      infoFormat
    ),
  });
  logger.info("Init logger.");
};

exports.info = (info) => {
  logger.info(info, "info");
};

const getUrlFromReq = (req) => _.get(req, "originalUrl");
const getUserFromReq = (req) =>
  _.get(req, "user", "") + " - " + _.get(req, "user");

exports.log = (err, level, req) => {
  const _level = level;
  const logContent = {
    level: _level,
    message: _.get(err, "message"),
    stack: _.get(err, "stack"),
  };
  if (_.get(err, "code")) logContent.code = _.get(err, "code");
  if (req) {
    logContent.url = getUrlFromReq(req);
    if (req.user) logContent.user = getUserFromReq(req);
  }
  logger.log(_level, logContent);
};

exports.error = (err, req) => {
  exports.log(err, "error", req);
};

/** @type {winston.Logger} */
exports.logger = logger;
