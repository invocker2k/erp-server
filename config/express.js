/**
 * Module dependencies.
 */
const fs = require("fs");
const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
// const session = require("express-session");
const compress = require("compression");
const cookieParser = require("cookie-parser");
const helmet = require("helmet");
const passport = require("passport");
const config = require("./config");
const path = require("path");
const logger = require("./logger");
const { allowCrossDomain } = require("../utils/cors.utils");
module.exports = function () {
  // Initialize express app
  const app = express();
  require("../models/index.models")();
  require("../mysql_models/index.model")();
  // Setting application local variables
  app.locals.title = config.app.title;
  app.locals.description = config.app.description;
  app.locals.keywords = config.app.keywords;

  try {
    const currentVersion = fs.readFileSync(
      path.resolve("./.cache-version"),
      "utf8"
    );
    app.locals.currentVersion = currentVersion;
  } catch (e) {
    logger.log(e);
    app.locals.currentVersion = 0;
  }

  // Should be placed before express.static
  app.use(
    compress({
      filter(req, res) {
        return /json|text|javascript|css/.test(res.getHeader("Content-Type"));
      },
      level: 9,
    })
  );
  // // default html
  // app.engine('html', require('ejs').renderFile);
  // app.set('view engine', 'html');
  // Showing stack errors
  app.set("showStackError", true);

  // Environment dependent middleware
  if (process.env.NODE_ENV === "development" || config.logger === "show") {
    // Enable logger (morgan)
    app.use(morgan("dev"));

    // Disable views cache
    app.set("view cache", false);
  } else if (process.env.NODE_ENV === "production") {
    app.locals.cache = "memory";
  }

  // Request body parsing middleware should be above methodOverride
  app.use(
    bodyParser.urlencoded({
      extended: true,
      limit: "100mb",
      parameterLimit: 50000,
    })
  );
  app.use(bodyParser.json({ limit: "100mb" }));

  // CookieParser should be above session
  app.use(cookieParser());

  //   const sessionStore = new RedisStore({
  //     host: config.redis.host,
  //     port: config.redis.port,
  //   });

  // Express MongoDB session storage
  // app.use(
  //   session({
  //     saveUninitialized: true,
  //     resave: true,
  //     //   secret: config.sessionSecret,
  //     cookie: { maxAge: 2592000000 },
  //     //   store: sessionStore,
  //   })
  // );

  // use passport session
  app.use(passport.initialize());
  // app.use(passport.session());

  // Use helmet to secure Express headers
  app.use(helmet());
  app.disable("x-powered-by");

  app.use(allowCrossDomain);

  //   const jwtAuthentication = require("../app/controllers/jwt-authentication.server.controller");
  //   const users = require("../app/controllers/users.server.controller");
  //   app.use(jwtAuthentication.checkJwtToken);
  //   app.use(users.checkAuthToken);

  // Globbing routing files
  //   config.getGlobbedFiles("./app/routes/**/*.js").forEach(function (routePath) {
  //     require(path.resolve(routePath))(app);
  //   });

  app.route("/health-check").get((req, res, next) => {
    res.status(200).send("ok");
  });
  const routes = require("../routes/index.router");
  routes(app);
  // Assume 'not found' in the error msgs is a 404.
  // this is somewhat silly, but valid, you can do whatever you like,
  // set properties, use instanceof etc.
  app.use(function (err, req, res, next) {
    // If the error object doesn't exists
    if (!err) return next();

    logger.log(err, "error", req);
    // Error page
    return res.status(500).send({
      message: err.message,
    });
  });

  // Assume 404 since no middleware responded
  app.use(function (req, res) {
    res.status(404).render("404", {
      url: req.originalUrl,
      error: "Not Found",
    });
  });

  //   Return Express server instance
  return app;
};
