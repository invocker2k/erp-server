"use strict";

const Sequelize = require("sequelize");
const config = require("./config");

const instance = new Sequelize({
  database: config.MYSQL_DBNAME,
  username: config.MYSQL_USER,
  password: config.MYSQL_PASSWORD,
  host: config.MYSQL_SERVER,
  port: config.MYSQL_PORT,
  dialect: "mysql",
  logging: false,
  operatorsAliases: false,
  // dialectOptions: {
  //     useUTC: false, //for reading from database
  // },
  // timezone: '+07:00', //for writing to database
  define: {
    freezeTableName: true,
    timestamps: true,
  },
  // query: {
  //     raw: true
  // }
});

module.exports = {
  Sequelize,
  instance,
};
