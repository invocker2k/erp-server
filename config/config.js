module.exports = {
  app: {
    title: "invocker Work",
    description: "Tools for invocker",
    keywords: "invocker",
  },
  db:
    process.env.MONGODB_URI ||
    `mongodb://${process.env.MONGODB_HOST || "localhost"}/${
      process.env.MONGODB_DB || "invocker"
    }`,
  redis: {
    host: process.env.REDIS_HOST || "localhost",
    port: process.env.REDIS_PORT || 6379,
  },
  logger: process.env.LOGGER || false,
  port: process.env.PORT || 3000,
  jwtSecret: process.env.JWT_SECRET || "invocker",
  MYSQL_SERVER: process.env.MYSQL_SERVER || "127.0.0.1",
  MYSQL_PORT: process.env.MYSQL_PORT || "3306",
  MYSQL_USER: process.env.MYSQL_USER || "user",
  MYSQL_PASSWORD: process.env.MYSQL_PASSWORD || "_iNVOCKER2K",
  MYSQL_DBNAME: process.env.MYSQL_DBNAME || "shop",
};
