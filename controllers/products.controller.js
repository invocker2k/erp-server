// const mongoose = require("mongoose");
// const _ = require("lodash");

// const Product = mongoose.model("products");

// exports.create = async function (req, res) {
//   const product = new Product(req.body);
//   await product
//     .save()
//     .then(async (product) => {
//       return res.jsonp(product);
//     })
//     .catch((err) => {
//       console.log(err);
//     });
// };
const Sequelizes = require("../utils/sequelize");
const { Products } = require("../mysql_models/product.model");
exports.create = async (req, res) => {
  const product = req.body;
  const [err, row] = await Sequelizes.create(Products, product);
  if (err || row === 0) {
    return res.status(404).jsonp({ message: err });
  }
  return res.status(200).jsonp({ message: "success" });
};

exports.listProduct = async (req, res) => {
  const [err, row] = await Sequelizes.findAll(Products);
  if (err) {
    return res.status(404).jsonp({ message: err });
  }
  return res.status(200).jsonp({ row });
};
