const mongoose = require("mongoose");
// const _ = require("lodash");

const User = mongoose.model("User");

exports.create = async (req, res) => {
  const user = new User(req.body);
  // user.displayName = `${user.lastName} ${user.firstName}`;+
  await user
    .save()
    .then(async (user) => {
      return res.jsonp(user);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.isUser = async (req, res) => {
  const userRequest = req.body;

  const user = await User.findOne({
    username: userRequest.username,
    password: userRequest.password,
  });

  return user ? res.status(200).send("ok") : res.status(401).send("auth");
};
