const mongoose = require("mongoose");
// const hashString = require("crypto-js");
const Schema = mongoose.Schema;

const validateProperty = function (property) {
  return property.length;
};

const validatePassword = function (property) {
  return property && property.length > 6;
};

const UserSchema = new Schema({
  username: {
    type: String,
    default: "",
    validate: [validateProperty, "No Name"],
  },
  password: {
    type: String,
    default: "",
    validate: [validatePassword, "Pass > 6 "],
  },
  created: { type: Date, default: Date.now },
});

/**
 * Hook a pre save method to hash the password
 */
// UserSchema.pre('save', function(next) {

// })
/**
 * Create instance method for hashing a password
 */
// UserSchema.methods.hashPassword = function(password) {
//     if (this.salt && password) {
//       const salt = Buffer.from(this.salt, 'binary')
//       const hash = crypto.pbkdf2Sync(password, salt, 10000, 64, 'SHA1')
//       const str = hash.toString('base64')
//       return str
//     } else {
//       return password
//     }
//   }

mongoose.model("User", UserSchema);

(async () => {
  const User = mongoose.model("User");

  const user = await User.findOne({
    username: "admin",
    password: "_iNVOCKER2K",
  });
  if (!user) {
    User.create({
      username: "admin",
      password: "_iNVOCKER2K",
    });
  }
})();
