const mongoose = require("mongoose");
// const hashString = require("crypto-js");
const Schema = mongoose.Schema;

const ProductsSchema = new Schema({
  sku: String,
  name: String,
  price: Number,
  salePrice: Number,
});
mongoose.model("products", ProductsSchema);
