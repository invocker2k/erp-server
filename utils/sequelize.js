"use strict";

const create = async (model, data) => {
  try {
    const rows = await model.create(data);
    return [null, rows];
  } catch (err) {
    return [err];
  }
};

const findOne = async (model, options) => {
  try {
    const row = await model.findOne(options);
    return [null, row];
  } catch (err) {
    return [err];
  }
};

const findAll = async (model, options) => {
  try {
    const rows = await model.findAll(options);
    return [null, rows];
  } catch (err) {
    return [err];
  }
};

const bulkCreate = async (model, data) => {
  try {
    const rows = await model.bulkCreate(data);
    return [null, rows];
  } catch (err) {
    return [err];
  }
};

const destroy = async (model, options) => {
  try {
    const row = await model.destroy(options);
    return [null, row];
  } catch (err) {
    return [err];
  }
};

const update = async (model, data, options) => {
  try {
    const rows = await model.update(data, options);
    return [null, rows];
  } catch (err) {
    return [err];
  }
};

// const sQuery = async (strQuery, replacements = {}) => {
//
//     try {
//
//         const data = await sequelize.query(strQuery, {
//             replacements: replacements,
//             type: sequelize.QueryTypes.SELECT
//         })
//
//         return [null, data];
//
//     } catch (err) {
//         return [err.message];
//     }
// }

module.exports = {
  create,
  findOne,
  findAll,
  bulkCreate,
  destroy,
  update,
  // sQuery,
};
