module.exports = (app) => {
  const user = require("../controllers/user.controller");
  app.route("/user/create").post(user.create);
  app.route("/user/auth").post(user.isUser);
};
