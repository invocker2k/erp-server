const fs = require("fs");

module.exports = (app) => {
  fs.readdirSync(__dirname).forEach((file) => {
    if (!file.endsWith(".js") || file === "index.router.js") return;

    const handler = require("./" + file);
    handler(app);
  });
};
