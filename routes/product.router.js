module.exports = (app) => {
  const products = require("../controllers/products.controller");
  app.route("/products/create").post(products.create);
  app.route("/products").get(products.listProduct);
};
